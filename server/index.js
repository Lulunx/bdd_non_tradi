let express = require('express');
let MongoClient = require('mongodb').MongoClient;
let assert = require('assert');
let fs = require('fs');
let url = require('url');
let querystring = require('querystring');

let database = 'mongodb://127.0.0.1:27017';
let dbName = 'airdata';
let client = new MongoClient(database);

let findDocuments = function (db, col, sort, callback) {
    // Get the documents collection
    let collection = db.collection(col);
    // Find some documents
    collection.find({}).sort(sort).toArray(function (err, docs) {
        assert.equal(err, null);
        callback(docs);
    });
}

let hostname = '127.0.0.1';
let port = 8080;
let app = express();

app.get('/*', function (req, res) {
    let page = url.parse(req.url).pathname;
    let params = querystring.parse(url.parse(req.url).query);
    if (page == "/") {
        page = "/index.html"
    }
    fs.readFile(__dirname + '/../client/' + page, function (err, data) {
        if (err) {
            fs.readFile(__dirname + '/../client/error.html', function (err, data) {
                res.end(data);
            })
        }
        else {
            res.end(data);
        }
    });
});


console.log("listening to " + port);
let io = require('socket.io').listen(app.listen(port), { log: false });

io.sockets.on('connection', function (socket) {
    socket.on('menu', function (data) {
        console.log("Received MENU REQUEST")
        client.connect(function (err) {
            assert.equal(err, null);
            let db = client.db(dbName);
            findDocuments(db, 'menu', { place: 1 }, function (data) {
                socket.emit("menu", data)
            });
        });
    });
    socket.on('airport', function (data) {
        console.log("Received airport REQUEST")
        client.connect(function (err) {
            assert.equal(err, null);
            let db = client.db(dbName);
            findDocuments(db, 'airports', {}, function (data) {
                socket.emit("airport", data)
            });
        });
    });
    socket.on('airportCapitals', function (data) {
        console.log("Received routes REQUEST")
        client.connect(function (err) {
            assert.equal(err, null);
            let db = client.db(dbName);
            let collection = db.collection("airports");
            // Find some documents
            collection.aggregate([{
                $lookup:
                {
                    from: "countries",
                    localField: "airportCity",
                    foreignField: "capital",
                    as: "capital"
                }
            }]).toArray(function (err, docs) {
                socket.emit("airportCapitals", docs)
            });
        });
    })
    socket.on('routes', function (data) {
        console.log("Received routes REQUEST")
        client.connect(function (err) {
            assert.equal(err, null);
            let db = client.db(dbName);
            let collection = db.collection("routes");
            // Find some documents
            collection.aggregate([{
                $lookup:
                {
                    from: "airports",
                    localField: "airportCodeSource",
                    foreignField: "airportIATA",
                    as: "source_airport"
                }
            },
            {
                $lookup:
                {
                    from: "airports",
                    localField: "airportCodeDestination",
                    foreignField: "airportIATA",
                    as: "dest_airport"
                }
            },
            { $skip: data.skip * 500 }, { $limit: 500 }]).toArray(function (err, docs) {
                socket.emit("routes", docs)
            });
        });
    })
})