let serverUrl = "http://127.0.0.1";
let serverPort = 8080;
let server = io.connect(serverUrl + ":" + serverPort);

server.on('menu', function (data) {
    makeMenu(data)
});
server.emit('menu')

let makeMenu = function (data) {
    var insert = d3.selectAll("#menu").selectAll("div");
    var menu = insert.data(data).enter();
    menu.append("div")
        .attr("class", "menu")
        .on("click", function (d) {
            window.location=d.URL;
        })
        .text(function (d) {
            return d.name
        });
}