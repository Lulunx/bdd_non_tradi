let skiped = 0;
let arcs = []
server.on('routes', function (data) {
  createFlights(data)
});

server.emit('routes', { skip: skiped })
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
var map = new Datamap({
  element: document.getElementById('container'),
  geographyConfig: {
    highlightOnHover: false,
    popupOnHover: false
  },
  arcConfig: {
    greatArc: true,
    animationSpeed: 1000,
    strokeWidth: 2,
    popupOnHover: true,
  }
});



async function createFlights(data) {
  let cond = (data.length == 0 || data == null)
  if (cond) {
    skiped = -1
  }
  skiped++
  server.emit('routes', { skip: skiped })
  if (!cond) {

    for (var i = 0; i < data.length; i++) {
      if (typeof data[i].source_airport[0] !== 'undefined' && data[i].dest_airport[0]) {

        var tarc = {
          origin: {
            latitude: data[i].source_airport[0].latitude,
            longitude: data[i].source_airport[0].longitude
          },
          destination: {
            latitude: data[i].dest_airport[0].latitude,
            longitude: data[i].dest_airport[0].longitude
          },
          text: data[i].source_airport[0].airportCity + " - " + data[i].dest_airport[0].airportCity
        }
        if (arcs.length >= 200) {
          arcs.shift();
        }
        arcs.push(tarc);
        await sleep(100);
        map.arc(arcs);
      }
    }
  }
}