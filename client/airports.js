
server.on('airport', function (data) {
    makeAirports(data, false)
});
server.on('airportCapitals', function (data) {
    makeAirports(data, true)
});
server.emit('airportCapitals')

document.getElementById("onlyCapitals").addEventListener("change", function () {
    if (this.checked) {
        server.emit('airportCapitals')
    }
    else {
        server.emit('airport')
    }
})

let map = new Datamap({
    element: document.getElementById('container'),
    geographyConfig: {
        highlightOnHover: false,
        popupOnHover: false
    },
    fills: {
        defaultFill: '#ABDDA4',
        red: 'red'
    },
    data: {
        'red': { fillKey: 'red' }
    }

});

let makeAirports = function (data, isCapital) {
    let airports = [];
    console.log(data)
    console.log(isCapital)
    for (var i = 0; i < data.length; i++) {
        data[i].radius = 4
        data[i].fillKey = 'red'
        if (!isCapital) {
            airports.push(data[i]);
        }
        else {
            if (data[i].capital.length > 0 && data[i].airportCountry == data[i].capital[0].country) {
                airports.push(data[i]);

            }
        }

    }
    map.bubbles(airports, {
        popupTemplate: function (geo, data) {
            return "<div class='hoverinfo'>Nom : " + data.airportName + "<br>Ville : " + data.airportCity + "<br>Pays : " + data.airportCountry + "</div>";
        }
    });
}