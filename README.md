Projet AirportData.
======
Le but est de permettre la visualisation des données des vols aériens.

Pour mettre en place ce projet, il suffit de récuperer le projet et de faire ce qu'il suit :

- [Dans un shell, se mettre dans ./server et exécuter `npm install`]
- [Dans un shell, se mettre dans ./data et exécuter `mongorestore -d airdata ./airdata`]
- [./server/index.js : ligne 8-10 pour la base de données et 22-23 pour l'URL.]
- [./client/menu.js : Pour modifier l'adresse du serveur si nécessaire (paramètres socket)]
- [Dans un shell, se mettre dans ./server et exécuter `node index.js`]
